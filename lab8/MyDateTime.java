public class MyDateTime {

    MyDatee date;
    MyTime time;


    public MyDateTime(MyDatee date, MyTime time) {
        this.date = date;
        this.time = time;
    }

    public void incrementDay() {
        date.incrementDay();
    }

    public void incrementHour() {
        time.incrementHour();
    }

    public void incrementHour(int diff) {
        int dayDiff = time.incrementHour(diff);
        if (dayDiff < 0){
            date.decrementDay(-dayDiff);
        }else {
            date.incrementDay(dayDiff);
        }

    }

    public void decrementHour(int diff) {
        incrementHour(-diff);
    }

    public void incrementMinute(int diff) {

        int dayDiff = time.incrementMinute(diff);
        if (dayDiff < 0){
            date.decrementDay(-dayDiff);
        }else {
            date.incrementDay(dayDiff);
        }
    }

    public void decrementMinute(int diff) {
        incrementMinute(-diff);
    }

    public void incrementYear(int diff) {
        date.incrementYear(diff);
    }

    public void decrementDay() {
        date.decrementDay();

    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }

    public void incrementDay(int diff) {
        date.incrementDay(diff);
    }

    public void decrementMonth(int diff) {
        date.decrementMonth(diff);
    }

    public void decrementDay(int diff) {
        date.decrementDay(diff);
    }

    public void incrementMonth(int diff) {
        date.incrementMonth(diff);
    }

    public void decrementYear(int diff) {
        date.decrementYear(diff);
    }

    public void incrementMonth() {
        date.incrementMonth();
    }

    public void incrementYear() {
        date.incrementYear();
    }

    public boolean isBefore(MyDateTime anotherDateTime) {
        if (date.isBefore(anotherDateTime.date)){
            return true;
        }else if (date.isAfter(anotherDateTime.date)){
            return false;
        }
        if (time.isBefore(anotherDateTime.time)){
            return true;
        }
        return false;


    }

    public boolean isAfter(MyDateTime anotherDateTime) {
        if (date.isAfter(anotherDateTime.date)){
            return true;
        }else if (date.isBefore(anotherDateTime.date)){
            return false;
        }
        if (time.isAfter(anotherDateTime.time)){
            return true;
        }
        return false;
    }

    public String dayTimeDifference(MyDateTime anotherDateTime) {


        int diff = 0;
        String dayTimeDifference = "";

        if (isBefore(anotherDateTime)){

            MyDatee tempMyDatee = new MyDatee(date.day,date.month+1,date.year);    // The main goal is create a temp MyDateTime but this object is also need
            MyTime tempMyTime = new MyTime(time.hour, time.minute);                // temp MyDatee and temp MyTİme objects.

            MyDateTime tempMyDateTime = new MyDateTime(tempMyDatee,tempMyTime);

            while (tempMyDateTime.isBefore(anotherDateTime)){                      //Increment minute while the MyDateTime is not equal another date
                tempMyDateTime.incrementMinute(1);                                 // so this will give me time difference.
                diff++;
            }

        }else if (isAfter(anotherDateTime)){                                       // same kind of operations for other situation

            MyDatee tempMyDatee = new MyDatee(anotherDateTime.date.day,anotherDateTime.date.month+1,anotherDateTime.date.year);
            MyTime tempMyTime = new MyTime(anotherDateTime.time.hour, anotherDateTime.time.minute);

            MyDateTime tempMyDateTime = new MyDateTime(tempMyDatee, tempMyTime);

            while (isAfter(tempMyDateTime)){
                tempMyDateTime.decrementMinute(1);
                diff++;
            }
        }

        int dayDiff = diff / (24 * 60);
        diff -= 24*60*dayDiff;
        int hourDiff = diff / 60;
        int minuteDiff = diff % 60;

        if (dayDiff != 0){
            dayTimeDifference += dayDiff + " day(s) ";
        }
        if (hourDiff != 0){
            dayTimeDifference += hourDiff + " hour(s) ";
        }
        if (minuteDiff != 0){
            dayTimeDifference += minuteDiff + " minute(s) ";
        }

        return dayTimeDifference + "\n";

    }

    public String toString(){
        return date.toString() + " " + time.toString();
    }

}
