public class FindPrime {
    public static void main(String[] args) {
        System.out.println("max = "+args[0]);
        int max = Integer.parseInt(args[0]);

        for(int number = 2; number < max; number++){
            boolean is_prime = true;
            int divisor = 2;
            while(is_prime && divisor < number){
                if(number % divisor == 0){
                    is_prime = false;
                }
                divisor++;
            }
            if(is_prime){
                System.out.print(number+",");
            }

        }


    }
}
