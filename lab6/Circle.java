public class Circle {
    int radius;
    Point center;

    public Circle(int radius, Point center) {
        this.radius = radius;
        this.center = center;
    }

    public double Area(){

        return Math.PI * radius * radius;

    }
    public double perimeter(){
        return 2 * Math.PI * radius;
    }

    public boolean intersect(Circle circle){


        // If the distance between the centers of two circles is less than the sum of the radius of the circles, they intersect.

        return (radius + circle.radius) > center.distanceFromaPoint(circle.center);

    }
}
