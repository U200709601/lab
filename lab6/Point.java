public class Point {


    int xCord;
    int yCord;

    public Point(int xCord, int yCord) {
        this.xCord = xCord;
        this.yCord = yCord;
    }
    public double distanceFromaPoint(Point point){

        int xdiff = xCord - point.xCord;
        int ydiff = yCord - point.yCord;
        double distance = Math.sqrt(xdiff * xdiff + ydiff * ydiff);

        return distance;

    }
}
