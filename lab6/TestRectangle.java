public class TestRectangle {
    public static void main(String[] args) {

        Point topLeft = new Point(10,10);

        Rectangle rectangle = new Rectangle(5,6,topLeft);

        System.out.println("Area = "+ rectangle.Area());
        System.out.println("Perimeter = "+ rectangle.perimeter());

        for (int i = 0; i < 4 ; i++){
            System.out.println("Corner"+i+" at x = "+rectangle.corners()[i].xCord + " at y = "+ rectangle.corners()[i].yCord);

        }


    }
}
