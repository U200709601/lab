import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		printBoard(board);
		int current_player = 0;
		int move_count = 0;

		while (move_count < 9) {

			System.out.print("Player "+ (current_player+1) + " enter row number:");
			int row = reader.nextInt();
			System.out.print("Player "+ (current_player+1) + " enter column number:");
			int col = reader.nextInt();
			if(col > 0 && col < 4 && row > 0 && row < 4 && board[row-1][col-1] == ' ') {
				if(current_player == 0) {
					board[row - 1][col - 1] = 'X';
				}else {
					board[row - 1][col - 1] = 'O';
				}
				printBoard(board);
				boolean win = checkBoard(board, row - 1, col - 1);
				if(win){
					System.out.println("Player"+ (current_player+1) + " has won the game");
					break;
				}
				current_player = (current_player + 1) % 2;
				move_count++;
			}else{
				System.out.println("Please enter valid command");

			}
		}
		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {

			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");
			}
			System.out.println();
			System.out.println("   -----------");

		}

	}
	public static boolean checkBoard(char[][] board, int row, int col){
		char symbol = board[row][col];
		boolean win = true;
		// check first conditions (column condition)
		for(int i = 0; i < 3; i++){
			if(board[row][i] != symbol){
				win = false;
			}
		}
		if(win){
			return true;
		}
		// check second condition (row condition)
		win = true;
		for(int i= 0;i < 3; i++){
			if(board[i][col] != symbol){
				win = false;
			}
		}
		if(win){
			return true;
		}
		//check third conditions (diagonals)

		if(row == col) {
			win = true;
			for (int i = 0, j = 0; i < 3; i++, j++){
				if(symbol != board[i][j]){
					win = false;
				}
			}
		}
		if (win){
			return true;
		}
		//the other diagonal

		if(row + col == 2){
			win = true;
			for(int i = 0,j = 2; i < 3; i++,j--){
				if(symbol != board[i][j]){
					win = false;
				}
			}
		}
		if (win){
			return true;
		}
		return false;


	}
}