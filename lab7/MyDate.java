public class MyDate {

    private int day;
    private int month;
    private int year;
    int[] maxDays = {31,29,31,30,31,30,31,31,30,31,30,31}; //jan is 0 , dec 11

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month - 1;
        this.year = year;
    }

    public void incrementDay() {

        int newDay = day +1;
        int maxDay = maxDays[month];

        if (newDay > maxDay){
            incrementMonth();
            day = 1;
        }else if (month == 1 && newDay == 29 && (year % 4) != 0){
            day = 1;
            incrementMonth();
        }else{
            day = newDay;
        }

    }
    public void incrementDay(int diff) {
        for (int i = 1 ; i <= diff ; i++){
            incrementDay();
        }
    }

    public void incrementYear(int diff) {
        year += diff;
    }

    public void decrementDay() {
        int newDay = day -1;

        if (newDay == 0){
            day = 31;
            decrementMonth();

        }else{
            day = newDay;
        }


    }

    public void decrementDay(int diff) {

        for (int i = 1; i <= diff; i++){
            decrementDay();
        }
    }

    public void decrementYear() {
        decrementYear(1);
    }
    public void incrementMonth() {
        incrementMonth(1);
    }
    public void incrementYear() {
        incrementYear(1);
    }
    public void decrementMonth(int diff) {

        for (int i = 1; i <= diff; i++){
            decrementMonth();
        }

    }

    public void decrementYear(int diff) {
        incrementYear(-diff);
        if (month == 1 && day == 29 && year % 4 != 0){
            day = 28;
        }
    }

    public void decrementMonth() {
        incrementMonth(-1);
    }

    public void incrementMonth(int diff) {
        int newMonth = (month + diff ) % 12;
        int yearDiff = 0;

        if (newMonth < 0){
            newMonth += 12;
            yearDiff = -1;
        }

        yearDiff += (month + diff) / 12;

        month = newMonth;
        year += yearDiff;

        if (day > maxDays[month]){
            day = maxDays[month];
            if (month == 1 && day == 29 && year % 4 != 0){
                day = 28;
            }
        }


    }

    public boolean isBefore(MyDate anotherDate){

        if(year < anotherDate.year ){
            return true;
        }else if(year > anotherDate.year){
            return false;
        }
        if (month < anotherDate.month){
            return true;
        }else if (month > anotherDate.month){
            return false;
        }
        if (day < anotherDate.day){
            return true;
        }else {
            return false;
        }


    }
    public boolean isAfter(MyDate anotherDate){

        if(year > anotherDate.year ){
            return true;
        }else if(year < anotherDate.year){
            return false;
        }
        if (month > anotherDate.month){
            return true;
        }else if (month < anotherDate.month){
            return false;
        }
        if (day > anotherDate.day){
            return true;
        }else {
            return false;
        }


    }
    public int dayDifference(MyDate anotherDate){

        MyDate tempDate = new MyDate(day,month+1,year);


        int count = 0;
        if (tempDate.isBefore(anotherDate)){
            while (tempDate.year != anotherDate.year || tempDate.month != anotherDate.month || tempDate.day != anotherDate.day){
                tempDate.incrementDay();
                count++;
            }
        }else if (tempDate.isAfter(anotherDate)){
            while (tempDate.year != anotherDate.year || tempDate.month != anotherDate.month || tempDate.day != anotherDate.day){
                tempDate.decrementDay();
                count++;
            }

        }

        return count;
    }
    public String toString(){

        return year +"-"+ ((month+1) < 10 ? "0" : "")+(month+1)+"-"+ (day < 10 ? "0" : "")+day;
    }


}
